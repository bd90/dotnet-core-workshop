using System;
using cw3_before.Models;
using Microsoft.AspNetCore.Mvc;

namespace cw3_before.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                Role = "User",
                Email = "test@test.pl",
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                PasswordHash = "none"
            };
            return Json(user);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody]User user)
        {
            return Json(user);
        }
    }
}