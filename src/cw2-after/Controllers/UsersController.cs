using System;
using cw2_after.Models;
using Microsoft.AspNetCore.Mvc;

namespace cw2_after.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                Role = "User",
                Email = "test@test.pl",
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                PasswordHash = "none"
            };
            return Json(user);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create(User user)
        {
            return Json(user);
        }
    }
}