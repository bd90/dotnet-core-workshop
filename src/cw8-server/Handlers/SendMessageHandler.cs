using System;
using System.Threading;
using System.Threading.Tasks;
using cw8_common;
using cw8_common.Messages;

namespace cw8_server.Handlers
{
    public class SendMessageHandler : IHandler<SendMessage>
    {   
        public Task HandleAsync(SendMessage @event, CancellationToken token)
        {          
            Console.WriteLine($"Receive: {@event.Message}");
            return Task.CompletedTask;
        }
    }
}