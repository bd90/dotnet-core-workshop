using System.Threading;
using System.Threading.Tasks;

namespace cw8_common
{
    public interface IHandler<in T> where T : IMessage
    {
        Task HandleAsync(T message, CancellationToken token);
    }
}