namespace cw8_common.Messages
{
    public class SendMessage : IMessage
    {
        public string Message { get; }
        public SendMessage(string message)
        {
            Message = message;
        }
    }
}