using System.Threading.Tasks;
using cw8_common.Messages;
using Microsoft.AspNetCore.Mvc;
using RawRabbit;

namespace cw8_client.Controllers
{
    public class MessageController : Controller
    {
        private readonly IBusClient _client;
        public MessageController(IBusClient client)
        {
            _client = client;
        }
        
        [HttpGet]
        [Route("/sendMessage")]
        public async Task<IActionResult> Create()
        {
            await _client.PublishAsync(new SendMessage("Test Message"));
        
            return Accepted();
        }
    }
}