namespace cw3_after.Dtos
{
    public class UserSignInResource
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}