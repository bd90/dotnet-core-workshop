using System;
using cw3_after.Dtos;
using cw3_after.Services.Auth;
using Microsoft.AspNetCore.Mvc;

namespace cw3_after.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IdentityController : Controller
    {
        private readonly IAuthService _authService;

        public IdentityController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        [Route("signUp")]
        public IActionResult SignUp(UserSignInResource resource)
        {
            var userId = Guid.NewGuid();
            _authService.SignUpAsync(userId, resource.Email, resource.Password, resource.Role);
            return Ok(new {userId});
        }
    }
}