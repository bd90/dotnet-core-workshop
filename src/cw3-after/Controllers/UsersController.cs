using System;
using cw3_after.Models;
using Microsoft.AspNetCore.Mvc;

namespace cw3_after.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Json(new User(Guid.NewGuid(), "test@test.pl", "user"));
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody]User user)
        {
            return Json(user);
        }
    }
}