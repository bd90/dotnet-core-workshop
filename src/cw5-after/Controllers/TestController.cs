using Microsoft.AspNetCore.Mvc;

namespace cw5_after.Controllers
{
    [Route("api/[controller]/[action]")] // matches localhost:xxxx/api/{nazwa controllera bez suffixu}/{nazwa akcji}
    public class TestController : Controller
    {
        [HttpGet]
        [Route("{id:int}/{name:alpha}")] // localhost:xxxx/api/test/index/{jakas liczba}/{jakis ciag znakow alpha-numerycznych}
        public IActionResult Index(int id, string name)
        {
            return Json(new {id, name});
        }
    }
}