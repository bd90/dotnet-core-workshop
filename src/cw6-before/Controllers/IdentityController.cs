using System;
using cw6_before.Dtos;
using cw6_before.Services.Auth;
using Microsoft.AspNetCore.Mvc;

namespace cw6_before.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IdentityController : Controller
    {
        private readonly IAuthService _authService;

        public IdentityController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        [Route("signIn")]
        public IActionResult SignIn(UserSignInResource resource)
        {
            var userId = Guid.NewGuid();
            _authService.SignUpAsync(userId, resource.Email, resource.Password, resource.Role);
            return Ok(new {userId});
        }
    }
}