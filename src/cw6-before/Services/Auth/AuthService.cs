using System;
using System.Threading.Tasks;
using cw6_before.Models;
using cw6_before.Persistence;
using Microsoft.AspNetCore.Identity;

namespace cw6_before.Services.Auth
{
    public interface IAuthService
    {
        Task SignUpAsync(Guid id, string email, string password, string role);
    }

    public class AuthService : IAuthService
    {
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly UsersRepository _usersRepository;

        public AuthService(IPasswordHasher<User> passwordHasher,
            UsersRepository usersRepository)
        {
            _passwordHasher = passwordHasher;
            _usersRepository = usersRepository;
        }
        
        public Task SignUpAsync(Guid id, string email, string password, string role)
        {
            var user = new User(id, email, role);
            user.SetPassword(password, _passwordHasher);
            _usersRepository.Add(user);
            return Task.CompletedTask;
        }
    }
}