using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace cw6_after.Hubs
{
    [Authorize]
    public class ApiHub : Hub
    {
        public async void SendToServer(object a)
        {
            await Clients.All.SendAsync("ReceiveMessage", JsonConvert.SerializeObject(a));
        }
        
        public async Task SendMessage(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", JsonConvert.SerializeObject(new { message }));
        }
    }
}