using System.Collections.Generic;
using cw6_after.Models;

namespace cw6_after.Persistence
{
    public class UsersRepository
    {
        private Dictionary<string, User> _users = new Dictionary<string, User>();

        public User GetByEmail(string email)
        {
            _users.TryGetValue(email, out var user);
            return user;
        }

        public void Add(User user)
        {
            _users.Add(user.Email, user);
        }
    }
}