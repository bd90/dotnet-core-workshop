namespace cw6_after.Dtos
{
    public class UserSignInResource
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}