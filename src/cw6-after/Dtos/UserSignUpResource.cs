namespace cw6_after.Dtos
{
    public class UserSignUpResource
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}