using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using cw6_after.Models;
using cw6_after.Persistence;
using Microsoft.AspNetCore.Identity;

namespace cw6_after.Services.Auth
{
    public interface IAuthService
    {
        Task SignUpAsync(Guid id, string email, string password, string role);
        Task<JsonWebToken> SignInAsync(string email, string password);
    }

    public class AuthService : IAuthService
    {
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly UsersRepository _usersRepository;
        private readonly IJwtHandler _jwtHandler;

        public AuthService(IPasswordHasher<User> passwordHasher,
            UsersRepository usersRepository,
            IJwtHandler jwtHandler)
        {
            _passwordHasher = passwordHasher;
            _usersRepository = usersRepository;
            _jwtHandler = jwtHandler;
        }
        
        public Task SignUpAsync(Guid id, string email, string password, string role)
        {
            var user = new User(id, email, role);
            user.SetPassword(password, _passwordHasher);
            _usersRepository.Add(user);
            return Task.CompletedTask;
        }
        
        public Task<JsonWebToken> SignInAsync(string email, string password)
        {
            var user = _usersRepository.GetByEmail(email);
            var jwt = _jwtHandler.CreateToken(user.Id.ToString(), user.Role, new Dictionary<string, string>());

            return Task.FromResult(jwt);
        }
    }
}