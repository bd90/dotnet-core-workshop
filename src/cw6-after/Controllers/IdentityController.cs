using System;
using System.Threading.Tasks;
using cw6_after.Dtos;
using cw6_after.Hubs;
using cw6_after.Services.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace cw6_after.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IdentityController : Controller
    {
        private readonly IAuthService _authService;
        private readonly IHubContext<ApiHub> _hub;

        public IdentityController(IAuthService authService, IHubContext<ApiHub> hub)
        {
            _authService = authService;
            _hub = hub;
        }

        [HttpPost]
        [Route("signUp")]
        public IActionResult SignUp(UserSignUpResource resource)
        {
            var userId = Guid.NewGuid();
            _authService.SignUpAsync(userId, resource.Email, resource.Password, resource.Role);
            return Ok(new {userId});
        }
        
        [HttpPost]
        [Route("signIn")]
        public IActionResult SignIn(UserSignInResource resource)
        {
            var temp = _authService.SignInAsync(resource.Email, resource.Password).Result;
            return Ok(temp);
        }
        
        [HttpGet]
        [Route("send")]
        public async Task<IActionResult> Send()
        {
            await _hub.Clients.All.SendAsync("SendMessage", new { status = "Ok" });
            return Accepted();
        }
    }
}