using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace cw6_after.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class SecretController : Controller
    {
        [Authorize(Policy = "Admins")]
        [HttpGet]
        public IActionResult Index()
        {
            return Json(new { lotto = "10,13,24,27,31,38" });
        }
    }
}